defmodule Capra.Repo.Migrations.CreateProducts do
  use Ecto.Migration

  def change do
    create table(:products) do
      add :name, :string
      add :description, :text
      add :slug, :string
      add :code, :string
      add :active, :boolean, default: false, null: false

      timestamps()
    end
    create unique_index(:products, [:slug])

  end
end
