defmodule Capra.Repo.Migrations.CreateOrganizations do
  use Ecto.Migration

  def change do
    create table(:organizations) do
      add :name, :string
      add :slug, :string
      add :active, :boolean, default: false, null: false

      timestamps()
    end

    create unique_index(:organizations, [:slug])
  end
end
