defmodule Capra.Repo.Migrations.CreateProjects do
  use Ecto.Migration

  def change do
    create table(:projects) do
      add :name, :string
      add :description, :text
      add :slug, :string
      add :active, :boolean, default: false, null: false

      timestamps()
    end
    create unique_index(:projects, [:slug])

  end
end
