## This file is a PO Template file.
##
## "msgid"s here are often extracted from source code.
## Add new translations manually only if they're dynamic
## translations that can't be statically extracted.
##
## Run "mix gettext.extract" to bring this file up to
## date. Leave "msgstr"s empty as changing them here as no
## effect: edit them in PO (.po) files instead.
msgid ""
msgstr ""

#, elixir-format
#: lib/capra_web/templates/pow/session/new.html.eex:34
msgid "Password"
msgstr ""

#, elixir-format
#: lib/capra_web/templates/pow/session/new.html.eex:43
msgid "Sign in"
msgstr ""

#, elixir-format
#: lib/capra_web/templates/page/index.html.eex:2
msgid "Welcome to %{name}!"
msgstr ""

#, elixir-format
#: lib/capra_web/templates/pow/session/new.html.eex:25
msgid "Email"
msgstr ""

#, elixir-format
#: lib/capra_web/pow/messages.ex:9
msgid "You need to sign in to see this page."
msgstr ""

#, elixir-format
#: lib/capra_web/pow/messages.ex:12
msgid "The provided login details did not work. Please verify your credentials, and try again."
msgstr ""
