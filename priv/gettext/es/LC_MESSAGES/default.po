## "msgid"s in this file come from POT (.pot) files.
##
## Do not add, change, or remove "msgid"s manually here as
## they're tied to the ones in the corresponding POT file
## (with the same domain).
##
## Use "mix gettext.extract --merge" or "mix gettext.merge"
## to merge POT files into PO files.
msgid ""
msgstr ""
"Language: es\n"
"Plural-Forms: nplurals=2\n"

#, elixir-format
#: lib/capra_web/templates/pow/session/new.html.eex:34
msgid "Password"
msgstr "Contraseña"

#, elixir-format
#: lib/capra_web/templates/pow/session/new.html.eex:43
msgid "Sign in"
msgstr "Iniciar sesión"

#, elixir-format
#: lib/capra_web/templates/page/index.html.eex:2
msgid "Welcome to %{name}!"
msgstr "Bienvenido %{name}"

#, elixir-format
#: lib/capra_web/templates/pow/session/new.html.eex:25
msgid "Email"
msgstr "Correo"

#, elixir-format
#: lib/capra_web/pow/messages.ex:9
msgid "You need to sign in to see this page."
msgstr "Necesita iniciar sesión para ver esta página"

#, elixir-format
#: lib/capra_web/pow/messages.ex:12
msgid "The provided login details did not work. Please verify your credentials, and try again."
msgstr "Credenciales incorrectas. Por favor verifique sus credenciales e intente de nuevo"
