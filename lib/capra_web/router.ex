defmodule CapraWeb.Router do
  use CapraWeb, :router
  use Pow.Phoenix.Router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :session do
    plug :put_layout, {CapraWeb.LayoutView , "session.html"}
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", CapraWeb do
    pipe_through :browser

    get "/", PageController, :index
  end

  scope "/" do
    pipe_through [:browser, :session]
    # pow_session_routes()

    pow_routes()
  end

  # Other scopes may use custom stacks.
  # scope "/api", CapraWeb do
  #   pipe_through :api
  # end
end
