defmodule CapraWeb.PageController do
  use CapraWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
