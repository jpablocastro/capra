defmodule CapraWeb.LayoutView do
  use CapraWeb, :view

  def danger_flash(conn) do
    render(CapraWeb.LayoutView, "_danger_flash.html", conn: conn)
  end

  def info_flash(conn) do
    render(CapraWeb.LayoutView, "_info_flash.html", conn: conn)
  end
end
