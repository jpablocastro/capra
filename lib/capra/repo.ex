defmodule Capra.Repo do
  use Ecto.Repo,
    otp_app: :capra,
    adapter: Ecto.Adapters.Postgres
end
