defmodule Capra.OrganizationStructure.Product do
  use Ecto.Schema
  import Ecto.Changeset
  import Capra.OrganizationStructure.SlugHelper

  schema "products" do
    field :active, :boolean, default: false
    field :description, :string
    field :name, :string
    field :slug, :string
    field :code, :string

    timestamps()
  end

  @doc false
  def changeset(product, attrs) do
    product
    |> cast(attrs, [:name, :description, :active, :code])
    |> validate_required([:name, :description, :active, :code])
    |> generate_slug
  end
end
