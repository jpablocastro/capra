defmodule Capra.OrganizationStructure.Project do
  use Ecto.Schema
  import Ecto.Changeset
  import Capra.OrganizationStructure.SlugHelper

  schema "projects" do
    field :active, :boolean, default: false
    field :description, :string
    field :name, :string
    field :slug, :string

    timestamps()
  end

  @doc false
  def changeset(project, attrs) do
    project
    |> cast(attrs, [:name, :description, :active])
    |> validate_required([:name, :description, :active])
    |> generate_slug
  end
end
