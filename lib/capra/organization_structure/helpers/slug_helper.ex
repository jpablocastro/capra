defmodule Capra.OrganizationStructure.SlugHelper do
  import Ecto.Changeset

  @doc """
  Genera el slug con el que se va acceder a la entidad
  """
  def generate_slug(changeset) do
    # TODO Considerar qué pasa si el slug se repite por alguna razón
    {_, name} = fetch_field(changeset, :name)

    case name do
      nil ->
        changeset

      name ->
        slug = Slug.slugify(name)

        changeset
        |> put_change(:slug, slug)
        |> validate_required([:slug])
        |> unique_constraint(:slug)
    end
  end
end
