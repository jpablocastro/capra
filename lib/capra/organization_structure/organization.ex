defmodule Capra.OrganizationStructure.Organization do
  use Ecto.Schema
  import Ecto.Changeset
  import Capra.OrganizationStructure.SlugHelper

  schema "organizations" do
    field :active, :boolean, default: false
    field :name, :string
    field :slug, :string

    timestamps()
  end

  @doc false
  def changeset(organization, attrs) do
    organization
    |> cast(attrs, [:name, :active])
    |> validate_required([:name, :active])
    |> generate_slug
  end
end
