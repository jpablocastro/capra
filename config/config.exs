# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :capra,
  ecto_repos: [Capra.Repo]

config :capra, CapraWeb.Gettext, default_locale: "es"

# Configures the endpoint
config :capra, CapraWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "6Vj0NyVwXP30cPOXZ5sHVPdDsn/9t6Ikn7k83QrNycHGNAC+r9BHCVv/OtPzJxkJ",
  render_errors: [view: CapraWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: Capra.PubSub, adapter: Phoenix.PubSub.PG2],
  live_view: [signing_salt: "dLn2EFAn"]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

config :capra, :pow,
  user: Capra.Users.User,
  repo: Capra.Repo,
  web_module: CapraWeb,
  messages_backend: CapraWeb.Pow.Messages

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
