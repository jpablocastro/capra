defmodule Capra.OrganizationStructureTest do
  use Capra.DataCase

  alias Capra.OrganizationStructure

  describe "organizations" do
    alias Capra.OrganizationStructure.Organization

    @valid_attrs %{active: true, name: "some name", slug: "some slug"}
    @update_attrs %{active: false, name: "some updated name", slug: "some updated slug"}
    @invalid_attrs %{active: nil, name: nil, slug: nil}

    def organization_fixture(attrs \\ %{}) do
      {:ok, organization} =
        attrs
        |> Enum.into(@valid_attrs)
        |> OrganizationStructure.create_organization()

      organization
    end

    test "list_organizations/0 returns all organizations" do
      organization = organization_fixture()
      assert OrganizationStructure.list_organizations() == [organization]
    end

    test "get_organization!/1 returns the organization with given id" do
      organization = organization_fixture()
      assert OrganizationStructure.get_organization!(organization.id) == organization
    end

    test "create_organization/1 with valid data creates a organization" do
      assert {:ok, %Organization{} = organization} = OrganizationStructure.create_organization(@valid_attrs)
      assert organization.active == true
      assert organization.name == "some name"
      assert organization.slug == "some-name"
    end

    test "create_organization/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = OrganizationStructure.create_organization(@invalid_attrs)
    end

    test "update_organization/2 with valid data updates the organization" do
      organization = organization_fixture()
      assert {:ok, %Organization{} = organization} = OrganizationStructure.update_organization(organization, @update_attrs)
      assert organization.active == false
      assert organization.name == "some updated name"
      assert organization.slug == "some-updated-name"
    end

    test "update_organization/2 with invalid data returns error changeset" do
      organization = organization_fixture()
      assert {:error, %Ecto.Changeset{}} = OrganizationStructure.update_organization(organization, @invalid_attrs)
      assert organization == OrganizationStructure.get_organization!(organization.id)
    end

    test "delete_organization/1 deletes the organization" do
      organization = organization_fixture()
      assert {:ok, %Organization{}} = OrganizationStructure.delete_organization(organization)
      assert_raise Ecto.NoResultsError, fn -> OrganizationStructure.get_organization!(organization.id) end
    end

    test "change_organization/1 returns a organization changeset" do
      organization = organization_fixture()
      assert %Ecto.Changeset{} = OrganizationStructure.change_organization(organization)
    end
  end

  describe "products" do
    alias Capra.OrganizationStructure.Product

    @valid_attrs %{active: true, description: "project's description", name: "project's name", slug: "some-slug", code: "MYP"}
    @update_attrs %{active: false, description: "some updated description", name: "project's name", slug: "some-updated-slug", code: "MYP"}
    @invalid_attrs %{active: nil, description: nil, name: nil, slug: nil}

    def product_fixture(attrs \\ %{}) do
      {:ok, product} =
        attrs
        |> Enum.into(@valid_attrs)
        |> OrganizationStructure.create_product()

      product
    end

    test "list_products/0 returns all products" do
      product = product_fixture()
      assert OrganizationStructure.list_products() == [product]
    end

    test "get_product!/1 returns the product with given id" do
      product = product_fixture()
      assert OrganizationStructure.get_product!(product.id) == product
    end

    test "create_product/1 with valid data creates a product" do
      assert {:ok, %Product{} = product} = OrganizationStructure.create_product(@valid_attrs)
      assert product.active == true
      assert product.description == "project's description"
      assert product.name == "project's name"
      assert product.slug == "projects-name"
    end

    test "create_product/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = OrganizationStructure.create_product(@invalid_attrs)
    end

    test "update_product/2 with valid data updates the product" do
      product = product_fixture()
      assert {:ok, %Product{} = product} = OrganizationStructure.update_product(product, @update_attrs)
      assert product.active == false
      assert product.description == "some updated description"
      assert product.name == "project's name"
      assert product.slug == "projects-name"
    end

    test "update_product/2 with invalid data returns error changeset" do
      product = product_fixture()
      assert {:error, %Ecto.Changeset{}} = OrganizationStructure.update_product(product, @invalid_attrs)
      assert product == OrganizationStructure.get_product!(product.id)
    end

    test "delete_product/1 deletes the product" do
      product = product_fixture()
      assert {:ok, %Product{}} = OrganizationStructure.delete_product(product)
      assert_raise Ecto.NoResultsError, fn -> OrganizationStructure.get_product!(product.id) end
    end

    test "change_product/1 returns a product changeset" do
      product = product_fixture()
      assert %Ecto.Changeset{} = OrganizationStructure.change_product(product)
    end
  end

  describe "projects" do
    alias Capra.OrganizationStructure.Project

    @valid_attrs %{active: true, description: "some description", name: "some name", slug: "some slug"}
    @update_attrs %{active: false, description: "some updated description", name: "some updated name", slug: "some updated slug"}
    @invalid_attrs %{active: nil, description: nil, name: nil, slug: nil}

    def project_fixture(attrs \\ %{}) do
      {:ok, project} =
        attrs
        |> Enum.into(@valid_attrs)
        |> OrganizationStructure.create_project()

      project
    end

    test "list_projects/0 returns all projects" do
      project = project_fixture()
      assert OrganizationStructure.list_projects() == [project]
    end

    test "get_project!/1 returns the project with given id" do
      project = project_fixture()
      assert OrganizationStructure.get_project!(project.id) == project
    end

    test "create_project/1 with valid data creates a project" do
      assert {:ok, %Project{} = project} = OrganizationStructure.create_project(@valid_attrs)
      assert project.active == true
      assert project.description == "some description"
      assert project.name == "some name"
      assert project.slug == "some-name"
    end

    test "create_project/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = OrganizationStructure.create_project(@invalid_attrs)
    end

    test "update_project/2 with valid data updates the project" do
      project = project_fixture()
      assert {:ok, %Project{} = project} = OrganizationStructure.update_project(project, @update_attrs)
      assert project.active == false
      assert project.description == "some updated description"
      assert project.name == "some updated name"
      assert project.slug == "some-updated-name"
    end

    test "update_project/2 with invalid data returns error changeset" do
      project = project_fixture()
      assert {:error, %Ecto.Changeset{}} = OrganizationStructure.update_project(project, @invalid_attrs)
      assert project == OrganizationStructure.get_project!(project.id)
    end

    test "delete_project/1 deletes the project" do
      project = project_fixture()
      assert {:ok, %Project{}} = OrganizationStructure.delete_project(project)
      assert_raise Ecto.NoResultsError, fn -> OrganizationStructure.get_project!(project.id) end
    end

    test "change_project/1 returns a project changeset" do
      project = project_fixture()
      assert %Ecto.Changeset{} = OrganizationStructure.change_project(project)
    end
  end
end
